$(document).ready(function () {


    $.fn.animateRotate = function (startDeg, stopDeg, duration, easing, complete, queue) {
        return this.each(function () {
            var $elem = $(this);
            $({deg: startDeg}).animate({deg: stopDeg}, {
                duration: duration,
                easing: easing,
                complete: complete,
                queue: queue,
                step: function (now) {
                    $elem.css({
                        transform: 'rotate(' + now + 'deg)',
                        opacity: 1
                    });
                    if(now == startDeg){
                        complete
                    }
                }
            });
        });
    };

    var elArrow = $(".div-arrow-btn__arrow");
    elArrow.animateRotate(137, 0, 1000, false,play(elArrow), true);


    function play(el) {
        el.closest(".test").next().animate({
            opacity: 1
        }, 4000);
    }


});

